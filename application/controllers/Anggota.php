<?php
defined('BASEPATH') OR die('No direct script access allowed!');

class Anggota extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        is_login();
        redirect_if_level_not('Admin');
        $this->load->model('Anggota_model', 'anggota');
    }

    public function index()
    {
        $data['anggota'] = $this->anggota->get_all();
        return $this->template->load('template', 'anggota/index', $data);
    }

    public function create()
    {
        $this->load->model('Divisi_model', 'divisi');
        $data['divisi'] = $this->divisi->get_all();
        return $this->template->load('template', 'anggota/create', $data);
    }

    public function store()
    {
        $post = $this->input->post();
        $data = [
            'nik' => $post['nik'],
            'nama' => $post['nama'],
            'telp' => $post['telp'],
            'divisi' => $post['divisi'],
            'email' => $post['email'],
            'username' => $post['username'],
            'password' => password_hash($post['password'], PASSWORD_DEFAULT),
        ];

        $result = $this->anggota->insert_data($data);
        if ($result) {
            $response = [
                'status' => 'success',
                'message' => 'Data anggota telah ditambahkan!'
            ];
            $redirect = 'anggota/';
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Data anggota gagal ditambahkan'
            ];
            $redirect = 'anggota/create';
        }
        
        $this->session->set_flashdata('response', $response);
        redirect($redirect);
    }

    public function edit()
    {
        $id_user = $this->uri->segment(3);
        $data['anggota'] = $this->anggota->find($id_user);
        return $this->template->load('template', 'anggota/edit', $data);
    }

    public function update()
    {
        $post = $this->input->post();
        $data = [
            'nik' => $post['nik'],
            'nama' => $post['nama'],
            'telp' => $post['telp'],
            'divisi' => $post['divisi'],
            'email' => $post['email'],
            'username' => $post['username'],
        ];

        if ($post['password'] !== '') {
            $data['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
        }

        $result = $this->anggota->update_data($post['id_user'], $data);
        if ($result) {
            $response = [
                'status' => 'success',
                'message' => 'Data Anggota berhasil diubah!'
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Data Anggota gagal diubah!'
            ];
        }
        
        $this->session->set_flashdata('response', $response);
        redirect('anggota');
    }

    public function destroy()
    {
        $id_user = $this->uri->segment(3);
        $result = $this->anggota->delete_data($id_user);
        if ($result) {
            $response = [
                'status' => 'success',
                'message' => 'Data anggota berhasil dihapus!'
            ];
        } else {
            $response = [
                'status' => 'error',
                'message' => 'Data anggota gagal dihapus!'
            ];
        }
        
        header('Content-Type: application/json');
        echo $response;
    }
}
